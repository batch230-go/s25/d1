// console.log("Hello World!");

// [SECTION] JSON Object
/*
    - JSON stands for JavaScript Object Notation
    - JSON is also used in other programming languages
    - Core JavaScript has a built-in JSON objects that contains methods for passing JSON objects
    - JSON is used for serializing different data types into bytes
*/

// JSON Object
/*
    - JSON also uses the key/value pairs like the object properties in JavaScript
    - Key/Property names should be enclosed with double quotes
    Syntax
        {
            "propertyA/keyA" : "valueA",
            "propertyB/keyB" : "valueB"
        }
*/
/*
    {
        "city" : "Quezon City",
        "province" : "Metro Manila",
        "country" : "Philippines"
    }
*/

// JSON Arrays
// Arrays in JSON are almost the same as arrays in JavaScript
// Arrays in JSON Object
/*
    "cities" : [
        {
            "city" : "Quezon City",
            "province" : "Metro Manila",
            "country" : "Philippines"
        },
        {
            "city" : "Manila City",
            "province" : "Metro Manila",
            "country" : "Philippines"
        },
        {
            "city" : "Makati City",
            "province" : "Metro Manila",
            "country" : "Philippines"
        }
    ]
*/

// [SECTION] JSON Methods
// The "JSON Object" contains methods for parsing and converting data in stringfied JSON
// JSON data is sent or received in text-only(String) format
// Converting Data Intro Stringified JSON

let batchesArr = [
    {
        batchName: 230,
        schedule: "Part Time",
    },
    {
        batchName: 240,
        schedule: "Full Time"
    }
];
console.log(batchesArr);
console.log("Result from stringify method: ");
// Syntax : JSON.stringify(arrayName/objectName);
console.log(JSON.stringify(batchesArr));





let data = JSON.stringify(
    {
        name: "John",
        age: 31,
        address: {
            city: "Manila",
            country: "Philippines"
        }
    }
);
console.log(data);

// User Details
/*
let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password: ");

let userDetails = JSON.stringify(
    {
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password
    }
);
console.log(userDetails);
*/


// [SECTION] converting Stringified JSON into JavaScript Objects

let batchesJSON = `[
    {
        "batchName" : 230,
        "schedule" : "Part Time"
    },
    {
        "batchName" : 240,
        "schedule" : "Full Time"
    }
]`;
console.log("batchesJSON content: ");
console.log(batchesJSON);

console.log("Result from parse method: ");
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);

let stringifiedObject = `
    {
        "name" : "John",
        "age" : 31,
        "address" : {
            "city" : "Manila",
            "country" : "Philippines"
        }
    }
`
console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));
let parseTest = JSON.parse(stringifiedObject);
console.log(parseTest);



//------------------------------------------------------------

let randomDice = Math.floor((Math.random() * 4) + 1);
console.log(randomDice);
switch(randomDice){
    case 1:
        console.log("You attack the enemy");
        break;
    case 2:
        console.log("You got hit by the enemy");
        break;
    case 3:
        console.log("You block your enemies attack");
        break;
    case 4:
        console.log("You flee");
}
